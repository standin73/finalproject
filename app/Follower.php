<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    protected $table = "follower";
    protected $fillable = ['user_id','post_id_follow'];

    public function coutFollower($user_id){
      return  $this->count()->where('user_id',$user_id);
    }
}
