<?php

namespace App\Http\Controllers;

use App\Comment;
use App\LikeComment;
use App\LikePost;
use App\Post;
use App\User;
use Illuminate\Http\Request;
Use File;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    protected $post;
    public function __construct()
    {
        $this->post = new Post(); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->post::all();
        
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $request->validate([
            'image'=>'required|image|mimes:jpeg,jpg,png|max:2048',
            'content'=>'required',
        ]);

        $imageName = time().uniqid().".". $request->image->extension();
        $request->image->move(public_path('images/post'),$imageName);

        $this->post = new Post();
        $this->post->image = $imageName;
        $this->post->content = $request->content;
        $this->post->user_id = $user_id;
        
        $this->post->save();
        
        return redirect('post/'.$this->post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->post->find($id);
        $comments = $data->comments()->select('comments.id as id','comments.*','users.id as uid','users.username')->join('users','users.id','=','comments.user_id')->paginate(10);
        $commentLikes = LikeComment::where('user_id',Auth::user()->id)->get();
        $data->commentLikes =$commentLikes;
        $user = User::find($data->user_id);
        $data['username'] = $user->username;
        
        $user_id = Auth::user()->id;
        $status = LikePost::where([
            'post_id'=>$id,
            'user_id'=>$user_id
        ])->get();
        $like=0;
        if(count($status)==1){
            $like = 1;
        }else{
            $like = 0;
        }
        return view('posts.show',compact('data','comments','like'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->post->findOrFail($id);
        return view('posts.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,jpg,png|max:2048',
            'content'=>'required',
        ]);

        if($request->has('image')){
            $data =   $this->post::find($id);
            $imageName = time().uniqid().".". $request->image->extension();
            $path = "images/post/";
            $oldImage = $data->image;

          
              $data->content=$request->content;
              $data->image = $imageName;

            $request->image->move(public_path('images/post'),$imageName);
            File::delete($path.$oldImage);

              $data->save();
            return redirect('/post'); 
        }else{
            $data =   $this->post::find($id);
            $data->content=$request->content;
            $data->save();
            return redirect('post/'.$id);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->post->find($id);
        $data->delete();
        return redirect('/post');
    }
}
