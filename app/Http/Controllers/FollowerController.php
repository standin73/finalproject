<?php

namespace App\Http\Controllers;

use App\Follower;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowerController extends Controller
{
    
    public function store(Request $request){
        $follow = new Follower();
        $follow['user_id'] = Auth::user()->id;
        $follow['user_id_follow'] = $request->user_id;

        $username = User::where('id',$request->user_id)->get()->first();
        
       
        $hasFollow = $follow->where(['user_id'=>Auth::user()->id,'user_id_follow'=>$request->user_id])->get();
       if(count($hasFollow)<=0){
            $follow->save();
       }

       return redirect("/profile/".$username->username);
    }

    public function destroy(Request $request){
        $follow = new Follower();
        $hasFollow = $follow->where(['user_id'=>Auth::user()->id,'user_id_follow'=>$request->user_id])->get()->first();
        if($hasFollow){
            $follow->destroy($hasFollow->id);
        }

       return redirect("profile/".$request->username);
    }
}
