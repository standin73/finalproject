<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\LikeComment;
use Illuminate\Http\Request;

class LikeCommentController extends Controller
{
    public function store(Request $request){
        $like = new LikeComment();
        // dd($request);
        $like->user_id = Auth::user()->id;
        $like->comment_id =$request->comment_id;
        $like->post_id =$request->post_id;
        $status =  $like->where([
            'comment_id'=>$request->comment_id,
            'user_id'=> Auth::user()->id
        ])->get();
        if(count($status)<=0){
            $like->save();
        }
        return redirect("post/".$request->post_id);
    }

    public function destroy(Request $request){
        $like = new LikeComment();
     
        $status =  $like->where([
            'comment_id'=>$request->comment_id,
            'user_id'=> Auth::user()->id
        ])->get()->first();
        
        if($status){
           $like->destroy($status->id);
        }
        return redirect("post/".$request->post_id);
    }
}
