<?php

namespace App\Http\Controllers;

use App\LikePost;
use Illuminate\Http\Request;

class LikePostController extends Controller
{
    public function store(Request $request,$post_id){
        $like = new LikePost();
        
        $like->user_id =$request->user_id;
        $like->post_id =$post_id;
        $status =  $like->where([
            'post_id'=>$post_id,
            'user_id'=>$request->user_id
        ])->get();
        if(count($status)<=0){
            $like->save();
        }
        return redirect('post/'.$post_id);
    }

    public function destroy(Request $request,$post_id){
        $like = new LikePost();
        $status =  $like->where([
            'post_id'=>$post_id,
            'user_id'=>$request->user_id
        ])->get()->first();
        
        if($status){
         
           $like->destroy($status->id);
        }
        return redirect('post/'.$post_id);
    }
}
