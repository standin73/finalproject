<?php

namespace App\Http\Controllers;

use App\Follower;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class ProfileController extends Controller
{
    public function index(){
        $id = Auth::user()->id;
        $user = User::find($id);
        $posts = Post::where('user_id',$id)->get();
        $isme=1;

        $followerCount =Follower::where('user_id_follow',$user->id)->count();
        $followingCount =Follower::where('user_id',$user->id)->count();
        $postCount = Post::where('user_id',$user->id)->count();
        
        $user['follower'] = $followerCount;
        $user['following'] = $followingCount;
        $user['post'] = $postCount;
        return view('profile.index',compact('user','posts','isme'));

    }

    
    public function edit($id)
    {
        $data = User::findOrFail($id);
        return view('profile.edit',compact('data'));
        // return view('profile.change',compact('data'));
    }

    public function show($username){
        $user=  User::where('username',$username)->get()->first();
        $cekIsFollow = Follower::where(['user_id'=> Auth::user()->id,'user_id_follow'=>$user->id])->get()->first();
        if($cekIsFollow){
            $user['isFollow'] = 1;
        }else{
            $user['isFollow'] = 0;
        };

        $followerCount =Follower::where('user_id_follow',$user->id)->count();
        $followingCount =Follower::where('user_id',$user->id)->count();
        $postCount = Post::where('user_id',$user->id)->count();
       
     
        if($user){
            if($user->id == Auth::user()->id){
                $isme=1;
            }else{
                $isme = 0;
            }
            $posts = Post::where('user_id',$user->id)->get();
        }else{

            return abort(404);
        }
        $user['follower'] = $followerCount;
        $user['following'] = $followingCount;
        $user['post'] = $postCount;
       
        return view('profile.index',compact('user','posts','isme'));
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =   User::find($id);
        if($data->email== $request->email){
            $emailValidate = ['required', 'string', 'email', 'max:255'];
        } else{
            $emailValidate =['required', 'string', 'email', 'max:255', 'unique:users'];
        }
        

        $request->validate([
            'image'=>'image|mimes:jpeg,jpg,png|max:2048',
            'bio'=>'required',
            'dob'=>'required',
            'name'=>'required',
            'email'=> $emailValidate
        ]);

        if($request->has('image')){
        
            $imageName = time().uniqid().".". $request->image->extension();
            $path = "images/avatar/";
            $oldImage = $data->avatar;

            // dd($oldImage);
              $data->email=$request->email;
              $data->name=$request->name;
              $data->dob=$request->dob;
              $data->bio=$request->bio;
              $data->avatar = $imageName;
              $request->image->move(public_path('images/avatar'),$imageName);
            if($oldImage!=='default.png'){
                // dd($path.$oldImage);
                File::delete($path.$oldImage);
            }

              $data->save();
            return redirect('/profile'); 
            }else{
            // $data =   User::find($id);
            $data->email=$request->email;
            $data->name=$request->name;
            $data->dob=$request->dob;
            $data->bio=$request->bio;
            $data->save();
            return redirect('/profile');
        }

    }

}
