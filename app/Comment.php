<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    protected $table = "comments";
    protected $fillable = ['user_id','post_id','content'];

    function getCommentByPost($postId){
        return $this->where('post_id',$postId)->get();
    }

    public function post(){
        return $this->belongsTo('App\Post');
    }
}
