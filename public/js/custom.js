window.onload = ()=>{
  const inputFill = document.querySelectorAll(".form-control.input-fill");
  
  const data = document.querySelector('#input-avatar')
  if(inputFill){
    inputFill.forEach(element=>{
      if(element.value !=""){
        element.parentNode.previousSibling.previousElementSibling.classList.add('filing')
      }else{
        element.parentNode.previousSibling.previousElementSibling.classList.remove('filing')
      }
      element.addEventListener('focus',e=>{
        e.preventDefault();
        if(e.target.classList.contains('date-input')){
          e.target.type = "date"
        }
        e.target.parentNode.previousSibling.previousElementSibling.classList.add('filing')
        // const target = e.target.parent.previousElementSibling
        // target.classList.add('filing');
    
      })
    
      element.addEventListener('focusout',e=>{
        e.preventDefault();
        if(e.target.value.length ==0){
          if(e.target.classList.contains('date-input')){
            e.target.type = "text"
          }
          e.target.parentNode.previousSibling.previousElementSibling.classList.remove('filing')
        }
      })
    })
  }
 
  function previewImg() {
    const preview = document.querySelector('img.profile-thumb');
    const file = data.files[0];
    const reader = new FileReader();
  
    reader.addEventListener("load", function () {
      // convert image file to base64 string
      preview.src = reader.result;
      
    }, false);
  
    if (file) {
      reader.readAsDataURL(file);
    }
  }
  if(data){

    data.addEventListener('change',previewImg);
  }


  tinymce.init({
    selector: 'textarea'
  });   
  
}