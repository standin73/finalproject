const inputImage = document.querySelector('#image');
function previewImg() {
  const preview = document.querySelector('#preview-image');
  const file = inputImage.files[0];
  const reader = new FileReader();

  reader.addEventListener("load", function () {
    // convert image file to base64 string
    preview.src = reader.result;
    
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}
if(inputImage){
  inputImage.addEventListener('change',previewImg);
}