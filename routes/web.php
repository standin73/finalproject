<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::resource('post', 'PostController');

Route::middleware('auth')->group(function () {
    Route::resource('post', 'PostController');
    Route::resource('profile', 'ProfileController');
    Route::resource('comment', 'ProfileController');
    Route::post('likepost/{id}', 'LikePostController@store');
    Route::delete('likepost/{id}', 'LikePostController@destroy');
    Route::post('comment/{id}', 'CommentController@store');
    Route::post('follow', 'FollowerController@store');
    Route::delete('follow', 'FollowerController@destroy');
    Route::delete('deletelikecomment', 'LikeCommentController@destroy');
    Route::post('likecomment', 'LikeCommentController@store');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/','HomeController@index')->name('home');
});


Auth::routes();

