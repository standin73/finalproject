@extends('layouts.app')
@push('style')
<link rel="stylesheet" href="{{asset('css/ig.css')}}">
@endpush
@section('content')
<header>

  <div class="container">

    <div class="profile">

      <div class="profile-image">

        <img src="{{asset('images/avatar').'/'.$user->avatar}}" alt="{{$user->username}}">

      </div>

      <div class="profile-user-settings">

        <h1 class="profile-user-name">{{$user->username}}</h1>
        @if ($isme==1)

        <a href="/profile/{{$user->id}}/edit" class="btn profile-edit-btn">Edit Profile</a>
        @else
        @if ($user->isFollow==1)
        <form class="d-inline" action="/follow" method="POST">
          @method('DELETE')
          @csrf
          <input type="hidden" name="user_id" value="{{$user->id}}">
          <button type="submit" class="btn profile-edit-btn btn-primary">Unfollow</button>
          <input type="hidden" name="username" value="{{$user->username}}">
        </form>
        @else
        <form class="d-inline" action="/follow" method="POST">
          @csrf
          <input type="hidden" name="user_id" value="{{$user->id}}">

          <button type="submit" class="btn profile-edit-btn btn-primary">Follow</button>
        </form>
        @endif

        @endif

        <button class="btn profile-settings-btn" aria-label="profile settings"><i class="fas fa-cog" aria-hidden="true"></i></button>

      </div>

      <div class="profile-stats">

        <ul>
          <li><span class="profile-stat-count">{{$user->post}}</span> posts</li>
          <li><span class="profile-stat-count">{{$user->follower}}</span> followers</li>
          <li><span class="profile-stat-count">{{$user->following}}</span> following</li>
        </ul>

      </div>

      <div class="profile-bio">

        <p><span class="profile-real-name">{{$user->name}} &nbsp; </span>{!!html_entity_decode($user->bio)!!}📷✈️🏕️</p>

      </div>

    </div>
    <!-- End of profile section -->

  </div>
  <!-- End of container -->

</header>

<main>

  <div class="container">

    <div class="gallery">
      @forelse($posts as $post)
      <div class="gallery-item">

        <img src="{{asset('images/post').'/'.$post->image}}" class="gallery-image" alt="Post">

        <a class=" gallery-item-info text-white" href="/post/{{$post->id}}">
          <ul>
            <li class="gallery-item-likes"><span class="visually-hidden">Likes:</span><i class="fas fa-heart" aria-hidden="true"></i> 56</li>
            <li class="gallery-item-comments"><span class="visually-hidden">Comments:</span><i class="fas fa-comment" aria-hidden="true"></i> 2</li>
          </ul>

        </a>


      </div>
      @empty
      <div class="col">
        <h3>Belum ada posting</h3>
      </div>
      @endforelse


    </div>
    <!-- End of gallery -->

    {{-- <div class="loader"></div> --}}

  </div>
  <!-- End of container -->

</main>
@endsection
