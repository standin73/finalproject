@extends('layouts.app')
@push('style')
<link rel="stylesheet" href="{{asset('css/ig.css')}}">
@endpush
@section('content')
<main>

  <div class="container">

    <div class="gallery">
      @forelse($posts as $post)
      <div class="gallery-item">

        <img src="{{asset('images/post').'/'.$post->image}}" class="gallery-image" alt="Post">

        <a class=" gallery-item-info text-white" href="/post/{{$post->id}}">
          <ul>
            <li class="gallery-item-likes"><span class="visually-hidden">Likes:</span><i class="fas fa-heart" aria-hidden="true"></i> 56</li>
            <li class="gallery-item-comments"><span class="visually-hidden">Comments:</span><i class="fas fa-comment" aria-hidden="true"></i> 2</li>
          </ul>

        </a>


      </div>
      @empty
      <div class="col">
        <h3>Belum ada posting</h3>
      </div>
      @endforelse


    </div>
    <!-- End of gallery -->

    {{-- <div class="loader"></div> --}}

  </div>
  <!-- End of container -->

</main>
@endsection
