@extends('layouts.app')
@section('content')
<div class="container mt-4">
  <form method="POST" enctype="multipart/form-data" action="/post/{{$data->id}}">
    @csrf
    @method('put')
    <input type="text" name="user_id" id="userid" value=1 class="d-none">
    <div class="form-group">
      <img src="{{asset('images/post')."/".$data->image}}" alt="preview" id="preview-image" class="imgage-thumbnail">
      <label for="image"></label>
      <input type="file" class="form-control-file  @error('image') is-invalid @enderror" id="image" name="image">
      @error('image')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    <div class="form-group">
      <label for="content">Example textarea</label>
      <textarea name="content" class="form-control @error('content') is-invalid @enderror" id=" content" rows="3">{{$data->content}}</textarea>
      @error('content')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>

    <button type="submit" class="btn btn-primary mb-2">Update</button>
  </form>
</div>
@endsection
