@extends('layouts.app')
@section('content')
<div class="container mt-4">
  <ul class="list-unstyled">
    @forelse ($posts as $post)
    <li class="media">
      <img width="100" src="{{asset('images/post')."/".$post->image}}" class="mr-3" alt="...">
      <div class="media-body">

        <p>{{Str::limit($post->content, 4, '...More')}}</p>
        <a href="/post/{{$post->id}}/edit" class="">Edit</a>
        <form action="/post/{{$post->id}}" method="post">
          @csrf
          @method('delete')
          <button type="submit">Delete</button>
        </form>

    </li>
    @empty

    @endforelse
  </ul>
</div>
@endsection
