@extends('layouts.app')
@push('style')
<link rel="stylesheet" href="{{asset('css')}}/post-detail.css">
@endpush
@section('content')
<div class="container">

  <div class="row justify-content-center">
    <div class="col col-md-6">
      <img src="{{asset('images/post').'/'.$data->image}}" class="w-100" alt="">
    </div>
    <div class="col-md-5">
      <div class="card  mb-3">
        <div class="card-header bg-transparent d-flex justify-content-between">
          <h5><a href="/profile/{{$data->username}}">{{($data->username)}} </a></h5>

          <span class="nav-item dropdown">
            <a class="dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">

            </a>
            @if ($data->user_id == (Auth::user()->id))
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" data-toggle="modal" data-target="#staticBackdrop" href="#">Edit Post</a>
              <button type="submit" class="dropdown-item">Delete Post</button>
            </div>
            @endif
          </span>
        </div>
        <div class="card-body text-success">

          <div class="card-title"><a href="/profile/{{$data->username}}">{{$data->username}} </a>{!!html_entity_decode($data->content)!!}</div>

        </div>

        <div class="card-footer bg-transparent ">
          @if ($comments->total()>0)
          <div class="row d-flex justify-content-between container">
            <span class="d-inline">{{$comments->total()}} Komentar</span> <span class="d-inline">{{ $comments->links() }}</span>
          </div>

          @else

          @endif
          <div class="buttongroup d-flex justify-content-between">
            <div class="left-group d-flex">
              @if ($like==1)
              <form action="/likepost/{{$data->id}}" method="POST">
                @method('DELETE')
                @csrf
                <input type="hidden" name="user_id" value="{{(Auth::user()->id)}}">

                <button class="btn btn-link"><i class="fas fa-heart text-danger"></i></button>
              </form>
              @else

              <form action="/likepost/{{$data->id}}" method="POST">
                @csrf
                <input type="hidden" name="user_id" value="{{(Auth::user()->id)}}">

                <button class="btn btn-link"><i class="far fa-heart"></i></button>
              </form>
              @endif

              <button data-toggle="modal" data-target="#comment" type="button" class="btn btn-link comment"><i class="far fa-comment"></i></button>
              <button class="btn btn-link"><i class="far fa-paper-plane"></i></button>
            </div>
            <div class="right-group">
              <button class="btn btn-link"><i class="far fa-bookmark"></i></i></button>
            </div>
          </div>
        </div>

        <div class="card-footer bg-transparent ">
          <ul class="list-group comment">
            @forelse ($comments as $key=> $item)
            <li class="list-group-item"><a href="/profile/{{$item->username}}">{{$item->username}} : </a> {!!html_entity_decode($item->content)!!}
              @if (count($data->commentLikes)>0)
              @foreach($data->commentLikes as $key => $value)

              @if ($value->comment_id == $item->id && $value->user_id==(Auth::user()->id))
              <div class="d-flex flex-column">
                <form action="/deletelikecomment" method="post">
                  @csrf
                  @method('delete')
                  <input type="hidden" name="comment_id" value="{{$item->id}}">
                  <input type="hidden" name="post_id" value="{{$data->id}}">
                  <button type="submit" class="btn btn-link"><i class="fas fa-heart text-danger"></i></button>
                </form>
                <a href="#" class="text-center"><i class="fas fa-reply"></i></a>
              </div>

              @else
              <div class="d-flex flex-column">
                <form action="/likecomment" method="post">
                  @csrf
                  <input type="hidden" name="comment_id" value="{{$item->id}}">
                  <input type="hidden" name="post_id" value="{{$data->id}}">
                  <button type="submit" class="btn btn-link"><i class="far fa-heart text-danger"></i></button>
                </form>
                <a href="#" class="text-center"><i class="fas fa-reply"></i></a>
              </div>
              @endif
              @endforeach
              @else
              <div class="d-flex flex-column">
                <form action="/likecomment" method="post">
                  @csrf
                  <input type="hidden" name="comment_id" value="{{$item->id}}">
                  <input type="hidden" name="post_id" value="{{$data->id}}">
                  <button type="submit" class="btn btn-link"><i class="far fa-heart text-danger"></i></button>
                </form>
                <a href="#" class="text-center"><i class="fas fa-reply"></i></a>
              </div>
              @endif


            </li>

            @empty
            <li class="list-group-item">Belum ada komentar</li>
            @endforelse
          </ul>
        </div>


      </div><!-- /.card -->

    </div>
  </div>

</div>

<!-- Modal Edit Post-->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Edit Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" enctype="multipart/form-data" action="/post/{{$data->id}}" novalidate>
        <div class="modal-body">

          @csrf
          @method('put')
          <div class="form-group">
            <label for="content">Post</label>
            <textarea required name="content" class="form-control @error('content') is-invalid @enderror" id=" content" rows="3">{{$data->content}}</textarea>
            @error('content')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>

        </div>
        <div class="modal-footer">
          {{-- <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

{{-- Modal Komentar --}}
<div class="modal fade" id="comment" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="commentLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="commentLabel">Edit Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" enctype="multipart/form-data" action="/comment/{{$data->id}}" novalidate>
        <div class="modal-body">

          @csrf
          <div class="form-group">
            <label for="content">Komentar Anda</label>
            <textarea required name="content" class="form-control @error('content') is-invalid @enderror" id=" content" rows="3"></textarea>
            @error('content')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>

        </div>
        <div class="modal-footer">
          {{-- <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
