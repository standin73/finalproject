@extends('layouts.app')

@section('content')
<div class="container mt-4">
  <div class="row justify-content-center">
    <form method="POST" enctype="multipart/form-data" action="/post">
      @csrf
      <input type="text" name="user_id" id="userid" value=1 class="d-none">
      <div class="form-group d-flex flex-column justify-content-center">
        <img src=" {{asset('coba/default.png')}}" alt="preview" id="preview-image" class="imgage-thumbnail profile-thumb">
        <label for="image"></label>
        <input type="file" class="form-control-file  @error('image') is-invalid @enderror" id="input-avatar" name="image">
        @error('image')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="content">Example textarea</label>
        <textarea name="content" class="form-control @error('content') is-invalid @enderror" id=" content" rows="3"></textarea>
        @error('content')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <button type="submit" class="btn btn-primary mb-2 w-100">Post</button>
    </form>
  </div>
</div>
@endsection
