@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-5">
      <div class="card">
        <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" novalidate>
          <div class="card-header profile-cover">
            <div class="profile-container col col-6 col-md-4 flex-column d-flex justify-content-center">

              <img src="{{asset('default.png')}}" class="profile-thumb img-thumbnail " alt="Avatar">
              <div class="custom-file img-upload-container d-flex justify-content-center ">
                <input type="file" id="input-avatar" class="d-none  @error('password') is-invalid @enderror" name="image" accept="image/*">
                @error('image')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
                <label for="input-avatar">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-camera" viewBox="0 0 16 16">
                    <path d="M15 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h1.172a3 3 0 0 0 2.12-.879l.83-.828A1 1 0 0 1 6.827 3h2.344a1 1 0 0 1 .707.293l.828.828A3 3 0 0 0 12.828 5H14a1 1 0 0 1 1 1v6zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z" />
                    <path d="M8 11a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7zM3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z" />
                  </svg>
                  <div>Ganti Profil</div>
                </label>
              </div>

            </div>
          </div>

          <div class="card-body">
            @csrf

            <label class="inputGroup ">
              <span class="label">Nama Lengkap</span>

              <div class="w-100">
                <input id="name" type="text" class="form-control input-fill @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" aria-label="Nama Lengkap" aria-required="true">

                @error('name')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </label>

            <label class="inputGroup ">
              <span class="label">Email anda</span>

              <div class="w-100">
                <input id="email" type="email" class="form-control input-fill @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" aria-label="Email anda" aria-required="true">

                @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </label>

            <label class="inputGroup ">
              <span class="label">Username (hanya huruf dan angka)</span>
              <div class="w-100">
                <input pattern="[a-zA-Z0-9]*" id="username" type="text" class="form-control input-fill @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" aria-label="Username" aria-required="true">

                @error('username')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </label>

            <label class="inputGroup ">
              <span class="label">Password</span>
              <div class="w-100">
                <input id="password" type="password" class="form-control input-fill @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password" aria-label="Password" aria-required="true">

                @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </label>

            <label class="inputGroup ">
              <span class="label">Password confirmation</span>
              <div class="w-100">
                <input id="password_confirmation" type="password" class="form-control input-fill @error('password_confirmation') is-invalid @enderror" name="password_confirmation" value="{{ old('password_confirmation') }}" required autocomplete="password_confirmation" aria-label="Password confirmation" aria-required="true">

                @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </label>

            <label class="inputGroup ">
              <span class="label">Tanggal Lahir</span>
              <div class="w-100">
                <input id="dob" type="text" class="form-control input-fill date-input @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob') }}" required autocomplete="dob" aria-label="Tanggal Lahir" aria-required="true">

                @error('dob')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </label>

            <label class="inputGroup ">
              <span class="label">Tentang Kamu</span>
              <div class="w-100">
                <textarea id="bio" class="form-control input-fill @error('bio') is-invalid @enderror" name="bio" required aria-label="Tentang Kamu">{{ old('bio') }}</textarea>

                @error('bio')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </label>


            <div class="form-group row mb-0">
              <div class="container">
                <button type="submit" class="btn btn-primary w-100">
                  {{ __('Register') }}
                </button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
{{-- </div> --}}
@endsection
